package com.wdjt.highwayCalculatorFast.modules.app.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.Query;

import com.wdjt.highwayCalculatorFast.modules.app.dao.RecordDao;
import com.wdjt.highwayCalculatorFast.modules.app.entity.RecordEntity;
import com.wdjt.highwayCalculatorFast.modules.app.service.RecordService;


@Service("recordService")
public class RecordServiceImpl extends ServiceImpl<RecordDao, RecordEntity> implements RecordService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RecordEntity> page = this.page(
                new Query<RecordEntity>().getPage(params),
                new QueryWrapper<RecordEntity>()
        );

        return new PageUtils(page);
    }

}