package com.wdjt.highwayCalculatorFast.modules.oss.controller;


import com.wdjt.highwayCalculatorFast.common.utils.R;
import com.wdjt.highwayCalculatorFast.modules.oss.service.FileStoreService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
@RequestMapping("oss/download")
public class DownloadController {
    private static final Logger log = LogManager.getLogger(DownloadController.class);
    @Autowired
    FileStoreService fileStoreService;

    @RequestMapping("/{path}/**")
    public void download(@PathVariable("path") String moduleBaseName, HttpServletRequest request, HttpServletResponse response) {
        final String path =
                request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE).toString();
        final String bestMatchingPattern =
                request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE).toString();

        String arguments = new AntPathMatcher().extractPathWithinPattern(bestMatchingPattern, path);

        String moduleName;
        if (null != arguments && !arguments.isEmpty()) {
            moduleName = moduleBaseName + '/' + arguments;
        } else {
            moduleName = moduleBaseName;
        }
//        return "module name is: " + moduleName;
        File file = new File(fileStoreService.getUploadFolderPath() + moduleName);
        try {
            // get your file as InputStream
            InputStream is = new FileInputStream(file);
            // copy it to response's OutputStream
            org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            log.info("Error writing file to output stream. Filename was '{}'", moduleName, ex);
            throw new RuntimeException("IOError writing file to output stream");
        }
    }
}
