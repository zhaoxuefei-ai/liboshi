package com.wdjt.highwayCalculatorFast.modules.fraud.controller;

import java.util.Arrays;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wdjt.highwayCalculatorFast.modules.fraud.entity.FraudCategoryEntity;
import com.wdjt.highwayCalculatorFast.modules.fraud.service.FraudCategoryService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.R;



/**
 * 案件的关键信息 规则结果表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 18:19:04
 */
@RestController
@RequestMapping("fraud/fraudcategory")
public class FraudCategoryController {
    @Autowired
    private FraudCategoryService fraudCategoryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("fraud:fraudcategory:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = fraudCategoryService.queryPage(params);

        return R.ok().put("page", page);
    }



    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("fraud:fraudcategory:info")
    public R info(@PathVariable("id") Integer id){
		FraudCategoryEntity fraudCategory = fraudCategoryService.getById(id);

        return R.ok().put("fraudCategory", fraudCategory);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("fraud:fraudcategory:save")
    public R save(@RequestBody FraudCategoryEntity fraudCategory){
		fraudCategoryService.save(fraudCategory);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("fraud:fraudcategory:update")
    public R update(@RequestBody FraudCategoryEntity fraudCategory){
		fraudCategoryService.updateById(fraudCategory);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("fraud:fraudcategory:delete")
    public R delete(@RequestBody Integer[] ids){
		fraudCategoryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
