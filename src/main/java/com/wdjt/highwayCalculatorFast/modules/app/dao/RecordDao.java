package com.wdjt.highwayCalculatorFast.modules.app.dao;

import com.wdjt.highwayCalculatorFast.modules.app.entity.RecordEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户计算记录表
 * 
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-18 00:14:33
 */
@Mapper
public interface RecordDao extends BaseMapper<RecordEntity> {

    int deleteBatch(Long[] ids);
}
