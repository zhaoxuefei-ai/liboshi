package com.wdjt.highwayCalculatorFast.modules.oss.controller;

import com.wdjt.highwayCalculatorFast.common.utils.MapUtils;
import com.wdjt.highwayCalculatorFast.common.utils.R;
import com.wdjt.highwayCalculatorFast.modules.oss.service.FileStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

@RestController
@RequestMapping("oss/upload")
public class UploadController {
@Autowired
    FileStoreService fileStoreService;


    @PostMapping("addImgs")
    public R addImgs(MultipartFile[] files){
        String downloadUrlPrefix = fileStoreService.getDownloadPrefix();
        ArrayList<String> strings = fileStoreService.addImgs(files, downloadUrlPrefix);
        return R.ok(new MapUtils().put("data",strings).put("error",0));
    }
}
