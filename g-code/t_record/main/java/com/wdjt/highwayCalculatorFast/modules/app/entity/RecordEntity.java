package com.wdjt.highwayCalculatorFast.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户计算记录表
 * 
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-18 00:14:33
 */
@Data
@TableName("t_record")
public class RecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 用户id
	 */
	private Long userId;
	/**
	 * 计算器名称
	 */
	private String category;
	/**
	 * 计算表数据
	 */
	private unknowType tableData;
	/**
	 * 创建时间
	 */
	private Date createTime;

}
