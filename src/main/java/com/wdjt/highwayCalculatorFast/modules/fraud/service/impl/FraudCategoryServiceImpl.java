package com.wdjt.highwayCalculatorFast.modules.fraud.service.impl;

import com.wdjt.highwayCalculatorFast.modules.fraud.service.FraudClassificationService;
import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.Query;

import com.wdjt.highwayCalculatorFast.modules.fraud.dao.FraudCategoryDao;
import com.wdjt.highwayCalculatorFast.modules.fraud.entity.FraudCategoryEntity;
import com.wdjt.highwayCalculatorFast.modules.fraud.service.FraudCategoryService;


@Service("fraudCategoryService")
public class FraudCategoryServiceImpl extends ServiceImpl<FraudCategoryDao, FraudCategoryEntity> implements FraudCategoryService {

    @Autowired
    FraudCategoryDao fraudCategoryDao;

    @Autowired
    FraudClassificationService fraudClassificationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FraudCategoryEntity> page = this.page(
                new Query<FraudCategoryEntity>().getPage(params),
                new QueryWrapper<FraudCategoryEntity>()
        );

        return new PageUtils(page);
    }


    @Override
    public List<FraudCategoryEntity> getFraudCategoryList(InsuranceEntity insuranceEntity) {
        List<FraudCategoryEntity> classification = fraudClassificationService.classification(insuranceEntity);
        return classification;
    }


}
