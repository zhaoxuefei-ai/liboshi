package com.wdjt.highwayCalculatorFast.modules.news.service.impl;

import com.wdjt.highwayCalculatorFast.common.utils.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.Query;

import com.wdjt.highwayCalculatorFast.modules.news.dao.NewsDao;
import com.wdjt.highwayCalculatorFast.modules.news.entity.NewsEntity;
import com.wdjt.highwayCalculatorFast.modules.news.service.NewsService;


@Service("newsService")
public class NewsServiceImpl extends ServiceImpl<NewsDao, NewsEntity> implements NewsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String category = (String) params.get("category");
        String title = (String) params.get("title");
        IPage<NewsEntity> page = this.page(
                new Query<NewsEntity>().getPage(params),
                new QueryWrapper<NewsEntity>()
                        .like(StringUtils.isNotBlank(title), "title", title)
                        .or(StringUtils.isNotBlank(category))
                        .like(StringUtils.isNotBlank(category), "category", category)
                        .orderByDesc("create_time")
        );
        return new PageUtils(page);
    }

    @Override
    public List<List<NewsEntity>> listCategoryWithTitle(Map<String, Object> params) {
        String title = (String) params.get("title");
        IPage<NewsEntity> page = this.page(
                new Query<NewsEntity>().getPage(params),
                new QueryWrapper<NewsEntity>()
                        .like(StringUtils.isNotBlank(title), "title", title)
                        .orderByDesc("update_time")

        );
        List<NewsEntity> records = page.getRecords();
        List<List<NewsEntity>> divider = ObjectUtils.divider(records, new Comparator<NewsEntity>() {
            @Override
            public int compare(NewsEntity o1, NewsEntity o2) {
                return o1.getCategory().compareTo(o2.getCategory());
            }
        });
        return divider;
    }

}
