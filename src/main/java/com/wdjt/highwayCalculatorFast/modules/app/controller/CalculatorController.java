package com.wdjt.highwayCalculatorFast.modules.app.controller;

import cn.hutool.core.io.resource.ClassPathResource;
import com.wdjt.highwayCalculatorFast.common.utils.R;
import com.wdjt.highwayCalculatorFast.modules.app.annotation.Login;
import com.wdjt.highwayCalculatorFast.modules.app.entity.CalculatorEntity;
import com.wdjt.highwayCalculatorFast.modules.app.service.CalculatorService;
import com.wdjt.highwayCalculatorFast.modules.sys.controller.AbstractController;
import com.wdjt.highwayCalculatorFast.modules.sys.entity.SysMenuEntity;
import com.wdjt.highwayCalculatorFast.modules.sys.service.ShiroService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;

import static com.mysql.jdbc.StringUtils.getBytes;
import static com.wdjt.highwayCalculatorFast.common.utils.ShiroUtils.getUserId;

@Login
@RestController
@RequestMapping("/app/cal")
public class CalculatorController extends AppController{
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    CalculatorService calculatorService;


    /**
     * 导航计算器列表
     */
    @GetMapping("/nav")
    public R nav(@RequestAttribute("userId") Long userId){
        // 能进入什么菜单 如果有个用户鸡贼,试出来了其他计算器的url怎么办?
        List<CalculatorEntity> calculatorList = calculatorService.getUserCalculatorList(userId);
//        System.out.println("calcu===>"+calculatorList);
        return R.ok().put("calculatorList", calculatorList);
    }

}
