package com.wdjt.highwayCalculatorFast.modules.insurance.dao;

import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 保险反欺诈系统 案件表 包括 档案组 (case是java 保留字 改为 insurance)
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 10:54:45
 */
@Repository
@Mapper
public interface InsuranceDao extends BaseMapper<InsuranceEntity> {

    int countSameGroupName(String groupName);

    int deleteByGroupName(String groupName);

    int countRiskPersonTime(String riskPersonName, String riskPersonIdNumber);

    int countRiskDriverTime(String riskDriverName, String riskDriverIdNumber);

    int countDrivingLicensePlateNumberTime(String drivingLicensePlateNumber);

}
