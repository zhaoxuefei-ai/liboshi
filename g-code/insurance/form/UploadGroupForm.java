package com.wdjt.highwayCalculatorFast.modules.insurance.form;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 组上传表单
 */
@Data
public class UploadGroupForm {
    private String tableFileName;
    private List<String> tableHeader;
    private List<Map<String,Object>> tableData;
}
