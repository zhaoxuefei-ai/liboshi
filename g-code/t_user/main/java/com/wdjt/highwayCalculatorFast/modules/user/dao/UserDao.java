package com.wdjt.highwayCalculatorFast.modules.user.dao;

import com.wdjt.highwayCalculatorFast.modules.user.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户
 * 
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-17 16:40:43
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
