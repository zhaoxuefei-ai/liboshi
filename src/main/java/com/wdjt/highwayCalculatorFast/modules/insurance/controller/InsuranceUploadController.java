package com.wdjt.highwayCalculatorFast.modules.insurance.controller;

import com.wdjt.highwayCalculatorFast.common.utils.R;
import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;
import com.wdjt.highwayCalculatorFast.modules.insurance.form.UploadGroupForm;
import com.wdjt.highwayCalculatorFast.modules.insurance.service.InsuranceService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;


/**
 * 保险反欺诈系统 案件表 包括 档案组 (case是java 保留字 改为 insurance)
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 10:54:45
 */
@RestController
@RequestMapping("insuranceUpload/insurance")
public class InsuranceUploadController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private InsuranceService insuranceService;

    /**
     * countSameGroupName
     */
    @RequestMapping("/hasSameGroupName")
    public R hasSameGroupName(@RequestParam Map<String, Object> params) {
        logger.debug(params.toString());
        String groupName = (String) params.get("GroupName");
        boolean hasSameGroupName = insuranceService.hasSameGroupName(groupName);
        return R.ok().put("hasSameGroupName", hasSameGroupName);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("insuranceUpload:insurance:info")
    public R info(@PathVariable("id") Integer id) {
        InsuranceEntity insurance = insuranceService.getById(id);

        return R.ok().put("insurance", insurance);
    }

    /**
     * 保存 saveGroup
     */
    @RequestMapping("/saveGroup")
    @RequiresPermissions("insuranceUpload:insurance:saveGroup")
    public R save(@RequestBody UploadGroupForm uploadGroupForm) {
        String groupName = uploadGroupForm.getTableFileName();
        // 转换 insuranceList
        List<Map<String, Object>> tableData = uploadGroupForm.getTableData();
        List<InsuranceEntity> insuranceList = new ArrayList<>();
        for (Map<String, Object> tableDatum : tableData) {
            insuranceList.add(InsuranceEntity.fromChineseKeyToEntity(tableDatum));
        }
        int success = insuranceService.saveGroup(groupName, insuranceList);
        Map result = new HashMap(2);
        result.put("total", tableData.size());
        result.put("success", success);
        return R.ok(result);
    }

    /**
     * 修改
     */
    @RequestMapping("/overrideGroup")
    @RequiresPermissions("insuranceUpload:insurance:overrideGroup")
    public R overrideGroup(@RequestBody UploadGroupForm uploadGroupForm) {
        String groupName = uploadGroupForm.getTableFileName();
        // 转换 insuranceList
        List<Map<String, Object>> tableData = uploadGroupForm.getTableData();
        List<InsuranceEntity> insuranceList = new ArrayList<>();
        for (Map<String, Object> tableDatum : tableData) {
            insuranceList.add(InsuranceEntity.fromChineseKeyToEntity(tableDatum));
        }
        Map result = insuranceService.overrideGroup(groupName,insuranceList);
        result.put("total", tableData.size());
        return R.ok(result);
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("insuranceUpload:insurance:delete")
    public R delete(@RequestBody Integer[] ids) {
        insuranceService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
