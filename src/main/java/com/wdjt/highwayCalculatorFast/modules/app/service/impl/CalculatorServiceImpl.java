package com.wdjt.highwayCalculatorFast.modules.app.service.impl;

import com.wdjt.highwayCalculatorFast.modules.app.entity.CalculatorEntity;
import com.wdjt.highwayCalculatorFast.modules.app.service.CalculatorService;
import com.wdjt.highwayCalculatorFast.modules.app.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("calculatorService")
public class CalculatorServiceImpl  implements CalculatorService  {

    @Autowired
    UserService userService;

    @Override
    public List<CalculatorEntity> getUserCalculatorList(Long userId) {
        //用户菜单列表
        List<CalculatorEntity> calculatorList = userService.queryAllCalculator(userId);

        return calculatorList;
    }
}
