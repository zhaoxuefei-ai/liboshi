package com.wdjt.highwayCalculatorFast.modules.user.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 用户
 * 
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-17 16:40:43
 */
@Data
@TableName("t_user")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long userId;
	/**
	 * 用户名
	 */
	private String username;
	/**
	 * 手机号
	 */
	private String company;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 允许计算器范围
	 */
	private String calExpireTimeList;
	/**
	 * 允许地址范围
	 */
	private String ipRangeList;
	/**
	 * 备注
	 */
	private String remark;

}
