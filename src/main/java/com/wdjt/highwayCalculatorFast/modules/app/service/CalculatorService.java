package com.wdjt.highwayCalculatorFast.modules.app.service;

import com.wdjt.highwayCalculatorFast.modules.app.entity.CalculatorEntity;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CalculatorService  {
    List<CalculatorEntity> getUserCalculatorList(Long userId);
}
