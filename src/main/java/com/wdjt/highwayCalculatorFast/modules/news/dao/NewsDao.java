package com.wdjt.highwayCalculatorFast.modules.news.dao;

import com.wdjt.highwayCalculatorFast.modules.news.entity.NewsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 新闻表
 * 
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-03 17:02:41
 */
@Mapper
public interface NewsDao extends BaseMapper<NewsEntity> {
	
}
