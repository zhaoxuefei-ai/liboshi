/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.wdjt.highwayCalculatorFast.modules.app.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wdjt.highwayCalculatorFast.modules.app.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;

/**
 * 用户
 *
 * @author Mark sunlightcs@gmail.com
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {

    void updateHeartbeat(Long userId, long date);
}
