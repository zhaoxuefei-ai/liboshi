/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.wdjt.highwayCalculatorFast.modules.app.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wdjt.highwayCalculatorFast.common.utils.IPUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * 用户
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-17 16:40:43
 */
@Data
@TableName("t_user")
public class UserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @TableId
    private Long userId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 所属公司
     */
    private String company;
    /**
     * 密码
     */
    @JsonIgnore
    private String password;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 到期时间
     */
    private Date expireTime;
    /*
     * 允许的计算器范围 以时间表示  默认 1997-01-01 00:00:00
     *	默认大小 12
     * */
    private String calExpireTimeList;

    /*
     * 允许地址范围
     *
     * */
    private String ipRangeList;
    /*
    * 用户使用时长 long
    * */
    private long useTime;
    /*
    * 上一次登录时间 long
    * */
    private long lastLoginTime;

    @TableField(exist = false)
    private boolean isOnline;

    //手机号
    private String mobile;
}
