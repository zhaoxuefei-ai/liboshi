package com.wdjt.highwayCalculatorFast.modules.news.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wdjt.highwayCalculatorFast.modules.news.entity.NewsEntity;
import com.wdjt.highwayCalculatorFast.modules.news.service.NewsService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.R;


/**
 * 新闻表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-03 17:02:41
 */
@RestController
@RequestMapping("news/news")
public class NewsController {
    @Autowired
    private NewsService newsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = newsService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 列表 种类 标题
     */
    @RequestMapping("/listCategoryWithTitle")
    public R listCategoryWithTitle(@RequestParam Map<String, Object> params) {
        params.put("page", "1");
        params.put("limit", "24");
        List<List<NewsEntity>> listCategoryWithTitle = newsService.listCategoryWithTitle(params);

        return R.ok().put("listCategoryWithTitle", listCategoryWithTitle);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Integer id) {
        NewsEntity news = newsService.getById(id);

        return R.ok().put("news", news);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("news:news:save")
    public R save(@RequestBody NewsEntity news) {
        newsService.save(news);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("news:news:update")
    public R update(@RequestBody NewsEntity news) {
        newsService.updateById(news);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("news:news:delete")
    public R delete(@RequestBody Integer[] ids) {
        newsService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
