package com.wdjt.highwayCalculatorFast.modules.news.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 新闻表
 * 
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-03 17:02:41
 */
@Data
@TableName("t_news")
public class NewsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 新闻类别
	 */
	private String category;
	/**
	 * 文章标题
	 */
	private String title;
	/**
	 * 文章内容
	 */
	private String content;
	/**
	 * 创建用户ID
	 */
	private Integer createUserId;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 修改用户ID
	 */
	private Integer updateUserId;
	/**
	 * 修改时间
	 */
	private Date updateTime;

}
