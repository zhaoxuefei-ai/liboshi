package com.wdjt.highwayCalculatorFast.modules.app.service.impl;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.corundumstudio.socketio.protocol.Packet;
import com.wdjt.highwayCalculatorFast.common.exception.RRException;
import com.wdjt.highwayCalculatorFast.modules.app.entity.HeartbeatForm;
import com.wdjt.highwayCalculatorFast.modules.app.entity.UserEntity;
import com.wdjt.highwayCalculatorFast.modules.app.entity.WebsocketClientEntity;
import com.wdjt.highwayCalculatorFast.modules.app.interceptor.AuthorizationInterceptor;
import com.wdjt.highwayCalculatorFast.modules.app.service.SocketEventService;
import com.wdjt.highwayCalculatorFast.modules.app.service.UserService;
import com.wdjt.highwayCalculatorFast.modules.app.utils.JwtUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * <p>
 * 消息事件处理
 * </p>
 *
 * @package: com.xkcoding.websocket.socketio.handler
 * @description: 消息事件处理
 * @author: yangkai.shen
 * @date: Created in 2018-12-18 18:57
 * @copyright: Copyright (c) 2018
 * @version: V1.0
 * @modified: yangkai.shen
 * @deprecated 连接时 如果没有sessionid 那么就在数据库里缓存一下
 * 如果有 那么就不操作数据库
 * // 重写
 * 连接和退出时 只返回操作结果和时间 不做处理
 * 当连接上的时候  每隔5秒前端发送心跳包
 * 后端 redis 统计 60次后存入mysql
 * 数据类型不变 都是long
 * timestamp
 */
@Component
@Slf4j
public class SocketEventServiceImpl implements SocketEventService {
    @Autowired
    private SocketIOServer server;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtUtils jwtUtils;

    // sessionID
    private static Map<String, WebsocketClientEntity> clientMap = new HashMap<>(100);

    // online user id
    private static Set<Long> onlineUserIdSet = new HashSet<>(100);


    /**
     * 添加connect事件，当客户端发起连接时调用
     */
    @Override
    @OnConnect
    public void onConnect(SocketIOClient client) {
        if (client != null) {
            String jwt = client.getHandshakeData().getSingleUrlParam("token");
            Claims claimByToken = jwtUtils.getClaimByToken(jwt);
            Long userId = null;
            if (claimByToken != null) {
                userId = Long.valueOf(claimByToken.getSubject());
            } else {
                log.error("connect error : empty jwt");
            }
        } else {
            log.error("客户端为空");
        }
    }

    /**
     * 添加disconnect事件，
     *
     * @param client 客户端对象
     */
    @Override
    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
        if (client != null) {
            UUID sessionId = client.getSessionId();
//            jwt tuken
            String jwt = client.getHandshakeData().getSingleUrlParam("token");
            // 模拟用户id 和token一致
            Claims claimByToken = jwtUtils.getClaimByToken(jwt);
            Long userId = null;
            WebsocketClientEntity websocketClientEntity;
            if (claimByToken != null) {
                userId = Long.valueOf(claimByToken.getSubject());
            } else {
                log.error("connect error : empty jwt and empty wce");
            }
            client.disconnect();
        } else {
            log.error("客户端为空");
        }
    }

    @Override
    @OnEvent(value = "heartbeat")
    public void setHeartbeat(SocketIOClient client, AckRequest request, HeartbeatForm form) {
        // 约定30秒一次
        if (client == null || form == null) {
            throw new RRException("NullPointerError");
        }
        Long uId = form.getUserId();
        Long t = form.getT();

        UserEntity user = userService.getById(uId);
        if (t - user.getLastLoginTime() >= 30 * 1000) {
            userService.updateHeartbeat(uId, t);
        }
    }

    @Override
    public boolean isOnline(Long userId) {
        return onlineUserIdSet.contains(userId);
    }

}
