/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.wdjt.highwayCalculatorFast.common.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 常量
 *
 * @author Mark sunlightcs@gmail.com
 */
public class Constant {
    /**
     * 超级管理员ID
     */
    public static final int SUPER_ADMIN = 1;
    /**
     * 当前页码
     */
    public static final String PAGE = "page";
    /**
     * 每页显示记录数
     */
    public static final String LIMIT = "limit";
    /**
     * 排序字段
     */
    public static final String ORDER_FIELD = "sidx";
    /**
     * 排序方式
     */
    public static final String ORDER = "order";
    /**
     * 升序
     */
    public static final String ASC = "asc";

    /**
     * 菜单类型
     *
     * @author chenshun
     * @email sunlightcs@gmail.com
     * @date 2016年11月15日 下午1:24:29
     */
    public enum MenuType {
        /**
         * 目录
         */
        CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 定时任务状态
     *
     * @author chenshun
     * @email sunlightcs@gmail.com
     * @date 2016年12月3日 上午12:07:22
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
        NORMAL(0),
        /**
         * 暂停
         */
        PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static String ip;
    public static String port;

    public static Map<String, String> chinaToEnglish = new HashMap<String, String>() {{
        put("集料筛分试验", "jlsfsy");
        put("智能级配设计", "znjpsj");
        put("集料密度试验（细容量瓶法）", "jlmdsy_xrlpf");
        put("集料密度试验 (细坍落筒法)", "jlmdsy_xtltf");
        put("理论最大相对<br />密度试验", "ll_zdxd_mdsy");
        put("智能配合比设计", "znphbsj");
        put("智能配合比<br />设计检验", "znphbsjjy");
        put("矿料级配检验（燃烧炉法）", "kljpjy_rslf");
        put("矿料级配检验（离心分离法）", "kljpjy_lxflf");
    }};
    public static Map<String, String> englishToChina = new HashMap<String, String>() {{
        put("jlsfsy", "集料筛分试验");
        put("znjpsj", "智能级配设计");
        put("jlmdsy_xrlpf", "集料密度试验（细容量瓶法）");
        put("jlmdsy_xtltf", "集料密度试验 (细坍落筒法)");
        put("ll_zdxd_mdsy", "理论最大相对<br />密度试验");
        put("znphbsj", "智能配合比设计");
        put("znphbsjjy", "智能配合比<br />设计检验");
        put("kljpjy_rslf", "矿料级配检验（燃烧炉法）");
        put("kljpjy_lxflf", "矿料级配检验（离心分离法）");
    }};

}
