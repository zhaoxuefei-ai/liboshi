package com.wdjt.highwayCalculatorFast.modules.fraud.service.impl;

import com.wdjt.highwayCalculatorFast.common.utils.DateUtils;
import com.wdjt.highwayCalculatorFast.modules.fraud.dao.FraudCategoryDao;
import com.wdjt.highwayCalculatorFast.modules.fraud.entity.FraudCategoryEntity;
import com.wdjt.highwayCalculatorFast.modules.fraud.service.FraudClassificationService;
import com.wdjt.highwayCalculatorFast.modules.insurance.dao.InsuranceDao;
import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class FraudClassificationServiceImpl implements FraudClassificationService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    FraudCategoryDao fraudCategoryDao;
    @Autowired
    InsuranceDao insuranceDao;

    @Override
    public List<FraudCategoryEntity> classification(InsuranceEntity insuranceEntity) {
//        fraudCategoryList.put(parent.getBaseEnglishName(),parent.getChildrenNames());
        List<FraudCategoryEntity> fraudCategoryList = new ArrayList<>();
//        夜间出险
        if (insuranceEntity.getAccidentTime() != null) {
            if (DateUtils.getCalender(insuranceEntity.getAccidentTime()).get(Calendar.HOUR_OF_DAY) >= 22
                    || DateUtils.getCalender(insuranceEntity.getAccidentTime()).get(Calendar.HOUR_OF_DAY) <= 5) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("夜间出险"));
            }
        }
//        延迟报案	报案时间-事故时间＞1小时
        if (insuranceEntity.getReportTime() != null && insuranceEntity.getAccidentTime() != null) {
            if (insuranceEntity.getReportTime().getTime() - insuranceEntity.getAccidentTime().getTime() >= 60 * 60 * 1000) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("延迟报案"));
            }
        }
//        人伤损失金额	人伤估损大于＞20000
        if (insuranceEntity.getHumanInjuryAmount() != null) {
            if (insuranceEntity.getHumanInjuryAmount().compareTo(BigDecimal.valueOf(20000L)) >= 0) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("人伤损失金额"));
            }
        }
//        整案估损金额	整案估损大于＞50000
        if (insuranceEntity.getOverallCaseAmount() != null) {
            if (insuranceEntity.getOverallCaseAmount().compareTo(BigDecimal.valueOf(50000L)) >= 0) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("整案估损金额"));
            }
        }
//        非现场报案	报案时间-事故时间>10分钟
        if (insuranceEntity.getReportTime() != null && insuranceEntity.getAccidentTime() != null) {
            if (insuranceEntity.getReportTime().getTime() - insuranceEntity.getAccidentTime().getTime() >= 10 * 60 * 1000) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("非现场报案"));
            }
        }
//        老人受伤	出险人年龄＞55
//	临床诊断含股骨颈骨折、髋关节、肱骨粗隆间骨折、肱骨外科颈骨折
//	诊断条数＜3
        if (insuranceEntity.getRiskPersonAge() != null && !insuranceEntity.getRiskPersonAge().equals("") && insuranceEntity.getClinicalDiagnosis() != null) {
            if (Integer.valueOf(insuranceEntity.getRiskPersonAge()) >= 55
                    && containKeyword(insuranceEntity.getClinicalDiagnosis(), Arrays.asList("股骨颈骨折", "髋关节", "肱骨粗隆间骨折", "肱骨外科颈骨折"))
                    && splitByComma(insuranceEntity.getClinicalDiagnosis()).length <= 3
            ) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("老人受伤"));
            }
        }
//        高坠伤	临床诊断含跟骨粉碎性骨折
//	诊断条数＜3
        if (insuranceEntity.getClinicalDiagnosis() != null) {
            if (containKeyword(insuranceEntity.getClinicalDiagnosis(), Arrays.asList("跟骨粉碎性骨折"))
                    && splitByComma(insuranceEntity.getClinicalDiagnosis()).length <= 3
            ) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("高坠伤"));
            }
        }
//        单纯性内脏损伤	出险人=男性
//	16＜出险人年龄＜50
//	临床诊断含（肝破裂，脾破裂、肠破裂、切割伤）
//	诊断条数＜3
        if (insuranceEntity.getRiskPersonSex() != null && insuranceEntity.getRiskPersonAge() != null && !insuranceEntity.getRiskPersonAge().equals("") && insuranceEntity.getClinicalDiagnosis() != null) {
            if (containKeyword(insuranceEntity.getRiskPersonSex(), Arrays.asList("男"))
                    && Integer.valueOf(insuranceEntity.getRiskPersonAge()) >= 16 && Integer.valueOf(insuranceEntity.getRiskPersonAge()) <= 50
                    && containKeyword(insuranceEntity.getClinicalDiagnosis(), Arrays.asList("肝破裂", "脾破裂", "肠破裂", "切割伤"))
                    && splitByComma(insuranceEntity.getClinicalDiagnosis()).length <= 3
            ) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("单纯性内脏损伤"));
            }
        }
//        单纯四肢伤	临床诊断含（截断、缺失、离断、趾骨骨折、指骨骨折）
//	诊断条数＜3
        if (insuranceEntity.getClinicalDiagnosis() != null) {
            if (containKeyword(insuranceEntity.getClinicalDiagnosis(), Arrays.asList("截断", "缺失", "离断", "趾骨骨折", "指骨骨折"))
                    && splitByComma(insuranceEntity.getClinicalDiagnosis()).length <= 3
            ) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("单纯四肢伤"));
            }
        }
//     高风险出险地	出险地点含（厂区、家属院、矿区、采石场、仓库、码头、物流园、公司、小区院内）
        if (insuranceEntity.getRiskLocation() != null) {
            if (containKeyword(insuranceEntity.getRiskLocation(), Arrays.asList(
                    "厂区", "家属院", "矿区", "采石场", "仓库", "码头", "物流园", "公司", "小区院内"))
            ) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("高风险出险地"));
            }
        }
        //双方姓名相似	出险人姓名与标的方都为两个字时，第一个字相同
        //	出险人姓名与标的方都为三个字及以上时，除第一个字相同之外还有一个字相同
        //注：标的方=被保险人、出险驾驶员 投保人、行驶证车主
        if (insuranceEntity.getRiskPersonName() != null) {
            if (similarName(insuranceEntity.getRiskPersonName(), Arrays.asList(
                    insuranceEntity.getInsuredPersonName(),
                    insuranceEntity.getRiskDriverName(),
                    insuranceEntity.getInsuranceApplicantPersonName(),
                    insuranceEntity.getDrivingLicenseOwnerName()))
            ) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("双方姓名相似"));
            }
        }
        // 异常出险原因     出险原因包含=倒车、逆行、人行道	    二者满足其一即可
        //出险经过包含=与人撞、撞人、撞行人、行人
        if (insuranceEntity.getRiskCause() != null && insuranceEntity.getRiskHistory() != null) {
            if (containKeyword(insuranceEntity.getRiskCause(), Arrays.asList("倒车", "逆行", "人行道"))
                    || containKeyword(insuranceEntity.getRiskHistory(), Arrays.asList("与人撞", "撞人", "撞行人", "行人"))) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("异常出险原因"));
            }
        }
        // 三者车外人颈腹损伤	损伤部位含颈部、腹部
        //	车种大类=小型家用车
        //	10000＜医疗费
        //	诊断条数＜3
        if (insuranceEntity.getDamageSite() != null && insuranceEntity.getCarType() != null && insuranceEntity.getMedicalFees() != null && insuranceEntity.getClinicalDiagnosis() != null) {
            if (containKeyword(insuranceEntity.getDamageSite(), Arrays.asList("颈部", "腹部"))
                    && containKeyword(insuranceEntity.getCarType(), Arrays.asList("小型家用车"))
                    && insuranceEntity.getMedicalFees().compareTo(new BigDecimal(10000)) >= 0
                    && splitByComma(insuranceEntity.getClinicalDiagnosis()).length <= 3) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("三者车外人颈腹损伤"));
            }
        }
        // 受害人多次出险	2＜出险人出险次数（姓名+身份证号）
        //	5000＜单人伤估损
        if (insuranceEntity.getRiskPersonName() != null && !insuranceEntity.getRiskPersonName().equals("")
                && insuranceEntity.getRiskPersonIdNumber() != null && !insuranceEntity.getRiskPersonIdNumber().equals("")
                && insuranceEntity.getHumanInjuryAmount() != null) {
            if (insuranceDao.countRiskPersonTime(insuranceEntity.getRiskPersonName(), insuranceEntity.getRiskPersonIdNumber()) >= 2
                    && insuranceEntity.getHumanInjuryAmount().compareTo(new BigDecimal(5000)) >= 0) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("受害人多次出险"));
            }
        }
        // 驾驶员多次人伤 2＜标的车驾驶员出险次数
        if (insuranceEntity.getRiskDriverName() != null && !insuranceEntity.getRiskDriverName().equals("")
                && insuranceEntity.getRiskDriverIdNumber() != null && !insuranceEntity.getRiskDriverIdNumber().equals("")) {
            if (insuranceDao.countRiskDriverTime(insuranceEntity.getRiskDriverName(), insuranceEntity.getRiskDriverIdNumber()) >= 2) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("驾驶员多次人伤"));
            }
        }
        //标的车多次人伤 2＜标的车出险次数
        if (insuranceEntity.getDrivingLicensePlateNumber() != null && !insuranceEntity.getDrivingLicensePlateNumber().equals("")) {
            if (insuranceDao.countDrivingLicensePlateNumberTime(insuranceEntity.getDrivingLicensePlateNumber()) >= 2) {
                fraudCategoryList.add(fraudCategoryDao.queryFraudCategory("标的车多次人伤"));
            }
        }
        return fraudCategoryList;
    }

    private boolean containKeyword(String source, List<String> keywords) {
        boolean isFind = false;
        for (String keyword : keywords) {
            isFind = isFind || source.contains(keyword);
        }
        return isFind;
    }

    private boolean similarName(String sourceName, List<String> targetNames) {
        boolean isSimilar = false;
        // 出险人姓名与标的方都为两个字时，第一个字相同
        //出险人姓名与标的方都为三个字及以上时，除第一个字相同之外还有一个字相同
        if (sourceName.length() == 2) {
            for (String targetName : targetNames) {
                if (targetName != null && targetName.length() == 2) {
                    isSimilar = isSimilar || sourceName.substring(0, 1).equals(targetName.substring(0, 1));
                }
            }
        } else if (sourceName.length() >= 3) {
            for (String targetName : targetNames) {
                if (targetName.length() >= 3) {
                    if (targetName != null && sourceName.substring(0, 1).equals(targetName.substring(0, 1))) {
                        // 还有一个字相同
                        String sourceSub = sourceName.substring(1);
                        String targetSub = targetName.substring(1);
                        for (int i = 0; i < targetSub.length(); i++) {
                            isSimilar = isSimilar || sourceName.contains(targetSub.charAt(i) + "");
                        }
                    }
                }
            }
        }
        return isSimilar;
    }

    private String[] splitByComma(String source) {
        // 只用中文走中文  只用英文走英文 如果不对校验 层要校验
        String[] split = null;
        if (source.contains(",")) {
            split = source.split(",");
        } else if (source.contains("，")) {
            split = source.split("，");
        }
        return split;
    }

    public static void main(String[] args) {
        FraudClassificationServiceImpl fraudClassificationService = new FraudClassificationServiceImpl();
//        System.out.println(fraudClassificationService.containKeyword("倒车", Arrays.asList("倒车", "逆行", "人行道")));
        System.out.println(fraudClassificationService.similarName("张吃的", Arrays.asList("张独都"))); // 2
    }

}
