package com.wdjt.highwayCalculatorFast.modules.insurance.service.impl;

import com.wdjt.highwayCalculatorFast.common.utils.DateUtils;
import com.wdjt.highwayCalculatorFast.common.utils.ShiroUtils;
import com.wdjt.highwayCalculatorFast.modules.fraud.dao.FraudCategoryDao;
import com.wdjt.highwayCalculatorFast.modules.fraud.service.FraudCategoryService;
import com.wdjt.highwayCalculatorFast.modules.sys.service.SysUserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.Query;

import com.wdjt.highwayCalculatorFast.modules.insurance.dao.InsuranceDao;
import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;
import com.wdjt.highwayCalculatorFast.modules.insurance.service.InsuranceService;


@Service("insuranceService")
public class InsuranceServiceImpl extends ServiceImpl<InsuranceDao, InsuranceEntity> implements InsuranceService {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    SysUserService sysUserService;
    @Autowired
    FraudCategoryService fraudCategoryService;
    @Autowired
    InsuranceDao insuranceDao;
    @Autowired
    FraudCategoryDao fraudCategoryDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
//        file_group report_code risk_city
        String key = (String) params.get("key");
        IPage<InsuranceEntity> page = this.page(
                new Query<InsuranceEntity>().getPage(params),
                new QueryWrapper<InsuranceEntity>()
                        .like(StringUtils.isNotBlank(key), "file_group", key)
                        .or()
                        .like(StringUtils.isNotBlank(key), "report_code", key)
                        .or()
                        .like(StringUtils.isNotBlank(key), "risk_city", key)
        );
        for (InsuranceEntity insuranceEntity : page.getRecords()) {
            // 处理ccuu
            insuranceEntity.setCreateUsername(sysUserService.queryUsernameByUserId(insuranceEntity.getCreateUserId().longValue()));
//            可能没有
            if (insuranceEntity.getUpdateUserId() != null) {
                insuranceEntity.setUpdateUsername(sysUserService.queryUsernameByUserId(insuranceEntity.getUpdateUserId().longValue()));
            }
            // 计算 规则 结果
            insuranceEntity.setFraudCategoryList(fraudCategoryService.getFraudCategoryList(insuranceEntity), fraudCategoryDao.getParentList());
        }
        return new PageUtils(page);
    }


    @Override
    public PageUtils queryPageAdv(Map<String, Object> params) {
//accidentTime: ["2019-08-07 00:00:00", "2019-09-19 00:00:00"]
//fileGroup: "11"
//insuranceName: ["个人意外伤害保险条款", "护理费及误工费用保险条款附加营养费"]
//insuredPersonName: "去1"
//reportCode: "11"
//reportTime: ["2019-08-01 00:00:00", "2019-09-13 00:00:00"]
//riskCity: ["重庆市", "三河市"]
        List<String> accidentTimeStr = params.get("accidentTime").toString().equals("") ? null : (List<String>) params.get("accidentTime");
        String fileGroup = (String) params.get("fileGroup");
        List<String> insuranceName = params.get("insuranceName").toString().equals("") ? null : (List<String>) params.get("insuranceName");
        String insuredPersonName = (String) params.get("insuredPersonName");
        String reportCode = (String) params.get("reportCode");
        List<String> reportTimeStr = params.get("reportTime").toString().equals("") ? null : (List<String>) params.get("reportTime");
        List<String> riskCity = params.get("riskCity").toString().equals("") ? null : (List<String>) params.get("riskCity");
        List<Date> reportTime = new ArrayList<>(2);
        List<Date> accidentTime = new ArrayList<>(2);
        if (reportTimeStr != null) {
            reportTimeStr.forEach(str -> reportTime.add(DateUtils.stringToDate(str, DateUtils.DATE_TIME_PATTERN)));
        }
        if (accidentTimeStr != null) {
            accidentTimeStr.forEach(str -> accidentTime.add(DateUtils.stringToDate(str, DateUtils.DATE_TIME_PATTERN)));
        }
        String key = (String) params.get("key");
        // 搜id
        int id = params.get("id") == null ? 0 : (int) params.get("id");
        IPage<InsuranceEntity> page = null;
        if (id != 0) {
            page = this.page(
                    new Query<InsuranceEntity>().getPage(params),
                    new QueryWrapper<InsuranceEntity>()
                            .eq("id", id)
            );
        } else {
            page = this.page(
                    new Query<InsuranceEntity>().getPage(params),
                    new QueryWrapper<InsuranceEntity>()
                            .like(StringUtils.isNotBlank(fileGroup), "file_group", fileGroup)
                            .like(StringUtils.isNotBlank(reportCode), "report_code", reportCode)
                            .like(StringUtils.isNotBlank(insuredPersonName), "insured_person_name", insuredPersonName)
                            .in(insuranceName != null, "insurance_name", insuranceName)
                            .in(riskCity != null, "risk_city", riskCity)
                            .between(reportTimeStr != null, "report_time", reportTime.isEmpty() ? null : reportTime.get(0), reportTime.isEmpty() ? null : reportTime.get(1))
                            .between(accidentTimeStr != null, "accident_time", accidentTime.isEmpty() ? null : accidentTime.get(0), accidentTime.isEmpty() ? null : accidentTime.get(1))
            );
        }
        for (InsuranceEntity insuranceEntity : page.getRecords()) {
            // 处理ccuu
            insuranceEntity.setCreateUsername(sysUserService.queryUsernameByUserId(insuranceEntity.getCreateUserId().longValue()));
//            可能没有
            if (insuranceEntity.getUpdateUserId() != null) {
                insuranceEntity.setUpdateUsername(sysUserService.queryUsernameByUserId(insuranceEntity.getUpdateUserId().longValue()));
            }
            // 计算 规则 结果
            insuranceEntity.setFraudCategoryList(fraudCategoryService.getFraudCategoryList(insuranceEntity), fraudCategoryDao.getParentList());
        }
        return new PageUtils(page);
    }

    @Override
    public boolean hasSameGroupName(String groupName) {
        int SameGroupNameCounter = insuranceDao.countSameGroupName(groupName);
        return SameGroupNameCounter != 0;
    }

    @Override
    public int saveGroup(String groupName, List<InsuranceEntity> insuranceList) {
        int successCount = 0;
        Date date = new Date();
        long userId = ShiroUtils.getUserId();
        for (InsuranceEntity insuranceEntity : insuranceList) {
            insuranceEntity.setFileGroup(groupName);
            insuranceEntity.setCreateTime(date);
            insuranceEntity.setCreateUserId(Integer.valueOf(Long.toString(userId)));
            if (insuranceDao.insert(insuranceEntity) == 1) {
                successCount++;
            }
        }
        return successCount;
    }

    @Override
    public Map overrideGroup(String groupName, List<InsuranceEntity> insuranceList) {

        Map result = new HashMap(3);
        int old = insuranceDao.countSameGroupName(groupName);
        int deleteResult = insuranceDao.deleteByGroupName(groupName);
        if (deleteResult != old) {
            int a = 1 / 0;
        }
        int success = saveGroup(groupName, insuranceList);
        result.put("success", success);
        result.put("old", old);
        return result;
    }


}
