package com.wdjt.highwayCalculatorFast.modules.news.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.modules.news.entity.NewsEntity;

import java.util.List;
import java.util.Map;

/**
 * 新闻表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-03 17:02:41
 */
public interface NewsService extends IService<NewsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<List<NewsEntity>> listCategoryWithTitle(Map<String, Object> params);
}

