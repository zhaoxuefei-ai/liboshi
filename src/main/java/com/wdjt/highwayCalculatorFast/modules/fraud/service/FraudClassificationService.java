package com.wdjt.highwayCalculatorFast.modules.fraud.service;

import com.wdjt.highwayCalculatorFast.modules.fraud.entity.FraudCategoryEntity;
import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;

import java.util.List;

public interface FraudClassificationService {

     List<FraudCategoryEntity> classification(InsuranceEntity insuranceEntity);

}
