package com.wdjt.highwayCalculatorFast.modules.app.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.modules.app.entity.RecordEntity;

import java.util.List;
import java.util.Map;

/**
 * 用户计算记录表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-18 00:14:33
 */
public interface RecordService extends IService<RecordEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void deleteBatch(Map<String, Object> params);
}

