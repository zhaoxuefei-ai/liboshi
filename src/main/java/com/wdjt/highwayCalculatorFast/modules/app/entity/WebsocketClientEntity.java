package com.wdjt.highwayCalculatorFast.modules.app.entity;

import lombok.Data;

import java.util.UUID;

@Data
public class WebsocketClientEntity {

    private long userId;
    private String jwt;
    private int totalLink;
    private UUID sessionId;
    private long startTime;

    public void increaseTotalLink(){
        this.totalLink++;
    }
    public void decreaseTotalLink(){
        this.totalLink--;
    }

    public WebsocketClientEntity(long userId, String jwt, int totalLink, UUID sessionId, long startTime) {
        this.userId = userId;
        this.jwt = jwt;
        this.totalLink = totalLink;
        this.sessionId = sessionId;
        this.startTime = startTime;
    }
}
