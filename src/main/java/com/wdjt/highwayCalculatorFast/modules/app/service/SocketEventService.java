package com.wdjt.highwayCalculatorFast.modules.app.service;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;
import com.wdjt.highwayCalculatorFast.modules.app.entity.HeartbeatForm;


public interface SocketEventService {


    @OnConnect
    void onConnect(SocketIOClient client);

    @OnDisconnect
    void onDisconnect(SocketIOClient client);


    @OnEvent(value = "heartbeat")
    void setHeartbeat(SocketIOClient client, AckRequest request, HeartbeatForm form);

    boolean isOnline(Long userId);
}
