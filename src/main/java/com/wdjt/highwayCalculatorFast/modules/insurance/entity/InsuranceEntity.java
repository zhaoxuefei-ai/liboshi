package com.wdjt.highwayCalculatorFast.modules.insurance.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.io.Serializable;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.wdjt.highwayCalculatorFast.common.utils.DateUtils;
import com.wdjt.highwayCalculatorFast.modules.fraud.entity.FraudCategoryEntity;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 保险反欺诈系统 案件表 包括 档案组 (case是java 保留字 改为 insurance)
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 10:54:45
 */
@Data
@TableName("t_insurance")
public class InsuranceEntity implements Serializable {
    private static Logger logger = LoggerFactory.getLogger(InsuranceEntity.class);


    private static final long serialVersionUID = 1L;
    /**
     * InsuranceEntity 根据规则所计算出来的结果
     * 1st String 是父级类标签 英文  ( 前端好操作
     * 2ed String[] 是子类显示标签 中文 应该放在list中
     */
    @TableField(exist = false)
    private Map<String, String[]> fraudCategoryList;


    /**
     * @param fraudCategoryList 都是child级别
     * @param parentList
     */
    public void setFraudCategoryList(List<FraudCategoryEntity> fraudCategoryList, List<FraudCategoryEntity> parentList) {
        logger.debug("开始setFraudCategoryList");
        // 根据对应的parentId 放置 child
        for (FraudCategoryEntity fraudCategoryEntity : fraudCategoryList) {
            for (FraudCategoryEntity parent : parentList) {
                if (parent != null) {
                    if (fraudCategoryEntity.getParentId() == parent.getId()) {
                        if (parent.getChildrenList() == null) {
                            parent.setChildrenList(new ArrayList<>());
                        }
                        parent.getChildrenList().add(fraudCategoryEntity);
                    }
                }
            }
        }
        Map<String, String[]> fraudCategoryMap = new HashMap<>();
        for (FraudCategoryEntity parent : parentList) {
            fraudCategoryMap.put(parent.getBaseEnglishName(), parent.getChildrenNames());
        }
        this.fraudCategoryList = fraudCategoryMap;
    }

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     * 档案组(excel 文件名称)
     */
    private String fileGroup;
    /**
     * 报案号
     */
    private String reportCode;
    /**
     * 报案时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date reportTime;
    /**
     * 报案人联系电话
     */
    private String reporterContactNumber;
    /**
     * 地区
     */
    private String region;
    /**
     * 出险城市
     */
    private String riskCity;
    /**
     * 出险地点
     */
    private String riskLocation;
    /**
     * 事故时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date accidentTime;
    /**
     * 报案-事故时间差（报案时间与事故时间的差值，可空，系统自动计算 分钟）
     */
    private Long reportAccidentInterval;
    /**
     * 出险类型
     */
    private String riskType;
    /**
     * 出险类型ssss
     */
    @TableField(exist = false)
    private String[] riskTypes;
    /**
     * 出险人姓名
     */
    private String riskPersonName;
    /**
     * 出险人年龄
     */
    private String riskPersonAge;
    /**
     * 出险人性别
     */
    private String riskPersonSex;
    /**
     * 出险人身份证号
     */
    private String riskPersonIdNumber;
    /**
     * 车种大类
     */
    private String carType;
    /**
     * 出险原因
     */
    private String riskCause;
    /**
     * 出险经过
     */
    private String riskHistory;
    /**
     * 损伤部位
     */
    private String damageSite;
    /**
     * 临床诊断
     */
    private String clinicalDiagnosis;
    /**
     * 医疗费
     */
    private BigDecimal medicalFees;
    /**
     * 保单号
     */
    private String insuranceNumber;
    /**
     * 案件处理人
     */
    private String caseHandlerName;
    /**
     * 保单起始日期
     */
    private Date insuranceStartDate;
    /**
     * 产品名称
     */
    private String productName;
    /**
     * 责任类型
     */
    private String typeOfResponsibility;
    /**
     * 责任类型ssss
     */
    @TableField(exist = false)
    private String[] typeOfResponsibilitys;
    /**
     * 险种名称
     */
    private String insuranceName;
    /**
     * 整案估损金额
     */
    private BigDecimal overallCaseAmount;
    /**
     * 人伤估损金额
     */
    private BigDecimal humanInjuryAmount;
    /**
     * 被保险人
     */
    private String insuredPersonName;
    /**
     * 被保险人身份证号
     */
    private String insuredPersonIdNumber;
    /**
     * 结案时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date closingTime;
    /**
     * 结案-报案（结案时间时间与报案时间的差值，可空，系统自动计算 分钟）
     */
    private Long closingReportNterval;
    /**
     * 首次理算开始处理时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date firstAdjustmentToStartProcessingTime;
    /**
     * 首次理算提交人
     */
    private String firstEstimateOfTheAuthor;
    /**
     * 审核状态
     */
    private String approvalStatus;
    /**
     * 结案金额区间(分隔符 - )
     */
    private String closingAmountInterval;
    /**
     * 索赔金额
     */
    private BigDecimal claimAmount;
    /**
     * 应剔除金额
     */
    private BigDecimal theAmountShouldBeRemoved;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 结案金额
     */
    private BigDecimal closingAmount;
    /**
     * 估损偏差
     */
    private BigDecimal estimatedLossDeviation;
    /**
     * 受益人材料
     */
    private String beneficiaryMaterial;
    /**
     * 残疾等级是否合理
     */

    private int isTheLevelOfDisabilityReasonable;
    /**
     * 事故责任是否合理
     */
    private int isTheLiabilityForTheAccidentReasonable;
    /**
     * 单证是否齐全
     */
    private int isTheDocumentComplete;
    /**
     * 沟通说明录入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private Date communicationInstructionsEntryTime;
    /**
     * 回访内容
     */
    private String returningContent;
    /**
     * 回访人
     */
    private String returningPersonName;
    /**
     * 其他问题
     */
    private String otherProblems;
    /**
     * 出险驾驶员
     */
    private String riskDriverName;
    /**
     * 出险驾驶员身份证号
     */
    private String riskDriverIdNumber;
    /**
     * 出险驾驶员户籍地
     */
    private String riskDriverResidence;
    /**
     * 投保人
     */
    private String insuranceApplicantPersonName;
    /**
     * 投保人身份证号
     */
    private String insuranceApplicantPersonIdNumber;
    /**
     * 投保人户籍地
     */
    private String insuranceApplicantPersonHouseholdRegistration;
    /**
     * 行驶证车主
     */
    private String drivingLicenseOwnerName;
    /**
     * 行驶证车主身份证号
     */
    private String drivingLicenseOwnerIdNumber;
    /**
     * 行驶证车主户籍地
     */
    private String drivingLicenseOwnerHouseholdRegistration;
    /**
     * 行驶证车牌号
     */
    private String drivingLicensePlateNumber;
    /**
     *
     */
    private Date createTime;
    /**
     *
     */
    private Integer createUserId;

    @TableField(exist = false)
    private String createUsername;
    @TableField(exist = false)
    private String updateUsername;
    /**
     *
     */
    private Date updateTime;
    /**
     *
     */
    private Integer updateUserId;

    public static InsuranceEntity fromChineseKeyToEntity(Map<String, Object> tableData) {
        InsuranceEntity insuranceEntity = new InsuranceEntity();
        for (String chineseKey : tableData.keySet()) {
            if (chineseKey.equals("报案号")) {
                insuranceEntity.setReportCode((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("报案时间")) {
                insuranceEntity.setReportTime(DateUtils.stringToDate((String) tableData.get(chineseKey), DateUtils.DATE_HOUR_MINUTE_PATTERN));
            }
            if (chineseKey.equals("报案人联系电话")) {
                insuranceEntity.setReporterContactNumber(((Long) tableData.get(chineseKey)).toString());
            }
            if (chineseKey.equals("地区")) {
                insuranceEntity.setRegion((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险城市")) {
                insuranceEntity.setRiskCity((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险地点")) {
                insuranceEntity.setRiskLocation((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("事故时间")) {
                insuranceEntity.setAccidentTime(DateUtils.stringToDate((String) tableData.get(chineseKey), DateUtils.DATE_HOUR_MINUTE_PATTERN));
            }
            if (chineseKey.equals("出险类型")) {
                insuranceEntity.setRiskType((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险人姓名")) {
                insuranceEntity.setRiskPersonName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险人年龄")) {
                insuranceEntity.setRiskPersonAge((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险人性别")) {
                insuranceEntity.setRiskPersonSex((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险人身份证号")) {
                insuranceEntity.setRiskPersonIdNumber((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("车种大类")) {
                insuranceEntity.setCarType((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险原因")) {
                insuranceEntity.setRiskCause((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险经过")) {
                insuranceEntity.setRiskHistory((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("损伤部位")) {
                insuranceEntity.setDamageSite((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("临床诊断")) {
                insuranceEntity.setClinicalDiagnosis((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("医疗费")) {
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Double")) {
                    insuranceEntity.setMedicalFees(BigDecimal.valueOf((Double) tableData.get(chineseKey)));
                } else if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    insuranceEntity.setMedicalFees(BigDecimal.valueOf(Long.valueOf((Integer) tableData.get(chineseKey))));
                } else {
                    logger.error("请更新代码" + tableData.get(chineseKey).getClass());
                }
            }
            if (chineseKey.equals("保单号")) {
                insuranceEntity.setInsuranceNumber((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("案件处理人")) {
                insuranceEntity.setCaseHandlerName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("保单起始日期")) {
                insuranceEntity.setInsuranceStartDate(DateUtils.stringToDate((String) tableData.get(chineseKey), DateUtils.DATE_TIME_PATTERN));
            }
            if (chineseKey.equals("产品名称")) {
                insuranceEntity.setProductName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("责任类型")) {
                insuranceEntity.setTypeOfResponsibility((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("险种名称")) {
                insuranceEntity.setInsuranceName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("整案估损金额")) {
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Double")) {
                    insuranceEntity.setOverallCaseAmount(BigDecimal.valueOf((Double) tableData.get(chineseKey)));
                } else if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    insuranceEntity.setOverallCaseAmount(BigDecimal.valueOf(Long.valueOf((Integer) tableData.get(chineseKey))));
                } else {
                    logger.error("请更新代码" + tableData.get(chineseKey).getClass());
                }
            }
            if (chineseKey.equals("人伤估损金额")) {
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Double")) {
                    insuranceEntity.setHumanInjuryAmount(BigDecimal.valueOf((Double) tableData.get(chineseKey)));
                } else if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    insuranceEntity.setHumanInjuryAmount(BigDecimal.valueOf(Long.valueOf((Integer) tableData.get(chineseKey))));
                } else {
                    logger.error("请更新代码" + tableData.get(chineseKey).getClass());
                }
            }
            if (chineseKey.equals("被保险人")) {
                insuranceEntity.setInsuredPersonName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("被保险人身份证号")) {
                insuranceEntity.setInsuredPersonIdNumber((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("结案时间")) {
                insuranceEntity.setClosingTime(DateUtils.stringToDate((String) tableData.get(chineseKey), DateUtils.DATE_HOUR_MINUTE_PATTERN));
            }
            if (chineseKey.equals("首次理算开始处理时间")) {
                int i = ((String) tableData.get(chineseKey)).lastIndexOf('.');
                if (i >= 0) {
                    tableData.put(chineseKey, ((String) tableData.get(chineseKey)).substring(0, i));
                }
                insuranceEntity.setFirstAdjustmentToStartProcessingTime(DateUtils.stringToDate((String) tableData.get(chineseKey), DateUtils.DATE_TIME_PATTERN));
            }
            if (chineseKey.equals("首次理算提交人")) {
                insuranceEntity.setFirstEstimateOfTheAuthor((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("审核状态")) {
                insuranceEntity.setApprovalStatus((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("结案金额区间")) {
                insuranceEntity.setClosingAmountInterval((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("索赔金额")) {
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Double")) {
                    insuranceEntity.setClaimAmount(BigDecimal.valueOf((Double) tableData.get(chineseKey)));
                } else if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    insuranceEntity.setClaimAmount(BigDecimal.valueOf(Long.valueOf((Integer) tableData.get(chineseKey))));
                } else {
                    logger.error("请更新代码" + tableData.get(chineseKey).getClass());
                }
            }
            if (chineseKey.equals("应剔除金额")) {
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Double")) {
                    insuranceEntity.setTheAmountShouldBeRemoved(BigDecimal.valueOf((Double) tableData.get(chineseKey)));
                } else if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    insuranceEntity.setTheAmountShouldBeRemoved(BigDecimal.valueOf(Long.valueOf((Integer) tableData.get(chineseKey))));
                } else {
                    logger.error("请更新代码" + tableData.get(chineseKey).getClass());
                }
            }
            if (chineseKey.equals("备注")) {
                insuranceEntity.setRemarks((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("结案金额")) {
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Double")) {
                    insuranceEntity.setClosingAmount(BigDecimal.valueOf((Double) tableData.get(chineseKey)));
                } else if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    insuranceEntity.setClosingAmount(BigDecimal.valueOf(Long.valueOf((Integer) tableData.get(chineseKey))));
                } else {
                    logger.error("请更新代码" + tableData.get(chineseKey).getClass());
                }
            }
            if (chineseKey.equals("估损偏差")) {
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Double")) {
                    insuranceEntity.setEstimatedLossDeviation(BigDecimal.valueOf((Double) tableData.get(chineseKey)));
                } else if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    insuranceEntity.setEstimatedLossDeviation(BigDecimal.valueOf(Long.valueOf((Integer) tableData.get(chineseKey))));
                } else {
                    logger.error("请更新代码" + tableData.get(chineseKey).getClass());
                }
            }
            if (chineseKey.equals("受益人材料")) {
                insuranceEntity.setBeneficiaryMaterial((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("残疾等级是否合理")) {
                insuranceEntity.setIsTheLevelOfDisabilityReasonable(tableData.get(chineseKey).toString().contains("不合理")?0:1);
            }
            if (chineseKey.equals("事故责任是否合理")) {
                insuranceEntity.setIsTheLiabilityForTheAccidentReasonable(tableData.get(chineseKey).toString().contains("不合理")?0:1);
            }
            if (chineseKey.equals("单证是否齐全")) {
                insuranceEntity.setIsTheDocumentComplete(tableData.get(chineseKey).toString().contains("不齐全")?0:1);
            }
            if (chineseKey.equals("沟通说明录入时间")) {
                insuranceEntity.setCommunicationInstructionsEntryTime(DateUtils.stringToDate((String) tableData.get(chineseKey), DateUtils.DATE_HOUR_MINUTE_PATTERN));
            }
            if (chineseKey.equals("回访内容")) {
                insuranceEntity.setReturningContent((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("回访人")) {
                insuranceEntity.setReturningPersonName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("其他问题")) {
                insuranceEntity.setOtherProblems((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险驾驶员")) {
                insuranceEntity.setRiskDriverName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险驾驶员身份证号")) {
                insuranceEntity.setRiskDriverIdNumber((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("出险驾驶员户籍地")) {
                insuranceEntity.setRiskDriverResidence((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("投保人")) {
                insuranceEntity.setInsuranceApplicantPersonName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("投保人身份证号")) {
                insuranceEntity.setInsuranceApplicantPersonIdNumber((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("投保人户籍地")) {
                insuranceEntity.setInsuranceApplicantPersonHouseholdRegistration((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("行驶证车主")) {
                insuranceEntity.setDrivingLicenseOwnerName((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("行驶证车主身份证号")) {
                insuranceEntity.setDrivingLicenseOwnerIdNumber((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("行驶证车主户籍地")) {
                insuranceEntity.setDrivingLicenseOwnerHouseholdRegistration((String) tableData.get(chineseKey));
            }
            if (chineseKey.equals("行驶证车牌号")) {
                insuranceEntity.setDrivingLicensePlateNumber((String) tableData.get(chineseKey));
            }
        }
        for (String chineseKey : tableData.keySet()) {
            if (chineseKey.equals("报案-事故间隔时间")) {
                // 自动计算
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    tableData.put(chineseKey, ((Integer) tableData.get(chineseKey)).doubleValue());
                }
                if (tableData.get(chineseKey) != null && ((Double) tableData.get(chineseKey)).compareTo(new Double(0)) != 0) {
                    // 这里会是天数
                    insuranceEntity.setReportAccidentInterval(Math.round((Double) tableData.get(chineseKey) * 24 * 60 * 60 * 1000));
                } else {
                    insuranceEntity.setReportAccidentInterval(insuranceEntity.getReportTime().getTime() - insuranceEntity.getAccidentTime().getTime());
                }
            }
            if (chineseKey.equals("结案-报案间隔时间")) {
                // 自动计算
                if (tableData.get(chineseKey).getClass().toString().contains("java.lang.Integer")) {
                    tableData.put(chineseKey, ((Integer) tableData.get(chineseKey)).doubleValue());
                }
                if (tableData.get(chineseKey) != null && ((Double) tableData.get(chineseKey)).compareTo(new Double(0)) != 0) {
                    // 天
                    insuranceEntity.setClosingReportNterval(Math.round((Double) tableData.get(chineseKey) * 24 * 60 * 60 * 1000));
                } else {
                    // 显示 分钟 输入 天
                    insuranceEntity.setClosingReportNterval(insuranceEntity.getClosingTime().getTime() - insuranceEntity.getReportTime().getTime());
                }
            }
        }
        return insuranceEntity;
    }

}
