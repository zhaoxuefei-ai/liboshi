package com.wdjt.highwayCalculatorFast.modules.oss.service;

import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * 获取服务器地址和图片
 * 返回服务器中的图片地址
 *
 * @author pzt
 */
public interface FileStoreService {
     String uploadFolderName = "object-store-service";

     String getDownloadPrefix() ;

     String getUploadFolderPath() ;

     ArrayList<String> addImgs(MultipartFile[] photos,String fileDownloadUrlProfix ) ;

}
