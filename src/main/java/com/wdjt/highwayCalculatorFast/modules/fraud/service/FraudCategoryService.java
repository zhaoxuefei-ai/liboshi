package com.wdjt.highwayCalculatorFast.modules.fraud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.modules.fraud.entity.FraudCategoryEntity;
import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;

import java.util.List;
import java.util.Map;

/**
 * 案件的关键信息 规则结果表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 18:19:04
 */
public interface FraudCategoryService extends IService<FraudCategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 这个应该在规则中维护
    ///**
    //     *  InsuranceEntity 根据规则所计算出来的结果
    //     *  1st String 是父级类标签 英文  ( 前端好操作
    //     *  2ed String[] 是子类显示标签 中文 应该放在list中
    //     */
    //    @TableField(exist=false)
    //    private Map<String,String> fraudCategoryList;
    List<FraudCategoryEntity> getFraudCategoryList(InsuranceEntity insuranceEntity);

}

