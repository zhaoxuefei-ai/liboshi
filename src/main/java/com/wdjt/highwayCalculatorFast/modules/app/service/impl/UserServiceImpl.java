/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.wdjt.highwayCalculatorFast.modules.app.service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wdjt.highwayCalculatorFast.common.exception.RRException;
import com.wdjt.highwayCalculatorFast.common.utils.*;
import com.wdjt.highwayCalculatorFast.common.validator.Assert;
import com.wdjt.highwayCalculatorFast.modules.app.dao.UserDao;
import com.wdjt.highwayCalculatorFast.modules.app.entity.CalculatorEntity;
import com.wdjt.highwayCalculatorFast.modules.app.entity.UserEntity;
import com.wdjt.highwayCalculatorFast.modules.app.form.LoginForm;
import com.wdjt.highwayCalculatorFast.modules.app.service.SocketEventService;
import com.wdjt.highwayCalculatorFast.modules.app.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {
    @Autowired
    SocketEventService socketEventService;

    @Override
    public UserEntity queryByUsername(String username) {
        return baseMapper.selectOne(new QueryWrapper<UserEntity>().eq("username", username));
    }

    @Override
    public long login(LoginForm form) {
        // 没有该用户
        UserEntity user = queryByUsername(form.getUsername());
        Assert.isNull(user, "用户名或密码错误");

        //密码错误
        if (!user.getPassword().equals(DigestUtils.sha256Hex(form.getPassword()))) {
            throw new RRException("用户名或密码错误");
        }

        // 用户已过期
        if (user.getExpireTime().before(new Date())) {
            throw new RRException("用户已过期");
        }
        // get ip
        String ipAddr = IPUtils.getIpAddr(HttpContextUtils.getHttpServletRequest());
        log.debug("ip: "+ipAddr);
        List<String> ipRangeList = Arrays.asList((user.getIpRangeList().split(";")))
                .stream().map(StringUtils::trim).collect(Collectors.toList());
        log.debug(ipRangeList.toString());
        for (String ipRange : ipRangeList) {
            if (!ipAddr.equals("0:0:0:0:0:0:0:1")
                    &&!ipAddr.equals("127.0.0.1")
                    &&!IPUtils.isIpInRange(ipAddr, ipRange)) {
                throw new RRException("用户IP不在ip白名单内");
            }
        }

        // 成功登陆
        user.setLastLoginTime(System.currentTimeMillis());
        updateById(user);
        return user.getUserId();
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = String.valueOf(params.get("key"));
        IPage<UserEntity> page = this.page(
                new Query<UserEntity>().getPage(params),
                new QueryWrapper<UserEntity>()
                        .like(!StringUtils.isBlank(key), "username", key)
                        .or()
                        .like(!StringUtils.isBlank(key), "company", key)
        );

        for (UserEntity userEntity : page.getRecords()) {
            userEntity.setOnline(socketEventService.isOnline(userEntity.getUserId()));
        }
        return new PageUtils(page);
    }

    @Override
    public List<CalculatorEntity> queryAllCalculator(Long userId) {
        // 返回用户可用的列表信息 过滤 只发送可使用的
        UserEntity user = getById(userId);
        String calExpireTimeList = user.getCalExpireTimeList();
        JSONArray jsonArray = JSON.parseArray(calExpireTimeList);
        System.out.println("jsonArray==="+jsonArray);
        List<CalculatorEntity> calculatorList = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            System.out.println("jsonObject===="+jsonObject);
            calculatorList.add(fillCalculatorEntity(jsonObject, Long.parseLong(i + "")));
        }
        return calculatorList;
    }

    @Override
    public boolean updatePassword(Long userId, String password, String newPassword) {
        UserEntity userEntity = new UserEntity();
        userEntity.setPassword(newPassword);
        return this.update(userEntity,
                new QueryWrapper<UserEntity>().eq("user_id", userId).eq("password", password));
    }

    @Override
    public void updateHeartbeat(Long userId, Long t) {
        baseMapper.updateHeartbeat(userId,t);
    }

    private CalculatorEntity fillCalculatorEntity(JSONObject jsonObject, long index) {
        CalculatorEntity calculatorEntity = new CalculatorEntity();
        calculatorEntity.setName(jsonObject.getString("name"));
        calculatorEntity.setMenuId(index);
        Date now = new Date();

//        System.out.println("jsonObject.getDate1==="+jsonObject.getDate("expire") != null);
//        System.out.println("jsonObject.getDate2==="+jsonObject.getDate("expire").after(now) );
        if (jsonObject.getDate("expire") != null && jsonObject.getDate("expire").after(now)) {
            calculatorEntity.setUrl(Constant.chinaToEnglish.get(calculatorEntity.getName()));
//            System.out.println("calculatorEntity===="+calculatorEntity);
//            System.out.println("Constant.chinaToEnglish.get(calculatorEntity.getName())===="+Constant.chinaToEnglish.get(calculatorEntity.getName()));
        }
        return calculatorEntity;
    }

}
