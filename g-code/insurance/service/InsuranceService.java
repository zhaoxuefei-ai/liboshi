package com.wdjt.highwayCalculatorFast.modules.insurance.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;

import java.util.List;
import java.util.Map;

/**
 * 保险反欺诈系统 案件表 包括 档案组 (case是java 保留字 改为 insurance)
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 10:54:45
 */
public interface InsuranceService extends IService<InsuranceEntity> {

    PageUtils queryPage(Map<String, Object> params);


    boolean hasSameGroupName(String groupName);

    int saveGroup(String groupName, List<InsuranceEntity> insuranceList);

    Map overrideGroup(String groupName, List<InsuranceEntity> insuranceList);

    PageUtils queryPageAdv(Map<String, Object> params);
}

