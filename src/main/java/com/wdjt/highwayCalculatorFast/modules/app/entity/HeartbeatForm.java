package com.wdjt.highwayCalculatorFast.modules.app.entity;

import lombok.Data;

@Data
public class HeartbeatForm {
    private Long userId;
    private Long t;
}
