package com.wdjt.highwayCalculatorFast.modules.fraud.dao;

import com.wdjt.highwayCalculatorFast.modules.fraud.entity.FraudCategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 案件的关键信息 规则结果表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 18:19:04
 */
@Mapper
@Repository
public interface FraudCategoryDao extends BaseMapper<FraudCategoryEntity> {


//    <!-- 根据父级 id 选择 所有子类-->
List<FraudCategoryEntity> getChildList(int parentId);

//    <!-- 选择 所有父类-->
List<FraudCategoryEntity> getParentList();

// 这个方法很慢  如何改进?
    @Cacheable("FraudCategory")
    FraudCategoryEntity queryFraudCategory(String fraudName);
}
