#!/bin/bash
mv car_api-1.0.jar car_api-1.0.jar.$(date +%Y%m%d)
mv car_api-1.0_*.jar car_api-1.0.jar
kill $( lsof -i:80 -t )
echo 'process in 80 has been killed'
rm -f nohup.out
nohup java -jar car_api-1.0.jar &
echo 'car_api in port 80'
sleep 1
tail -f nohup.out
