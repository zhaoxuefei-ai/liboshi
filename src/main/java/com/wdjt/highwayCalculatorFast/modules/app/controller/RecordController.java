package com.wdjt.highwayCalculatorFast.modules.app.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.wdjt.highwayCalculatorFast.common.exception.RRException;
import com.wdjt.highwayCalculatorFast.common.utils.HttpContextUtils;
import com.wdjt.highwayCalculatorFast.modules.app.annotation.Login;
import com.wdjt.highwayCalculatorFast.modules.app.entity.RecordEntity;
import com.wdjt.highwayCalculatorFast.modules.app.interceptor.AuthorizationInterceptor;
import com.wdjt.highwayCalculatorFast.modules.app.service.RecordService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.R;



/**
 * 用户计算记录表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-18 00:14:33
 */
@RestController
@RequestMapping("/app/record")
public class RecordController extends AppController {
    @Autowired
    private RecordService recordService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("app:record:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = recordService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * 列表 myList
     */
    @Login
    @RequestMapping("/myList")
    public R myList(@RequestParam Map<String, Object> params){
        // my
        Long userId = (Long) HttpContextUtils.getHttpServletRequest().getAttribute(AuthorizationInterceptor.USER_KEY);
        params.put("userId",userId);
        PageUtils page = recordService.queryPage(params);

        System.out.println("--------------------"+page);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("app:record:info")
    public R info(@PathVariable("id") Long id){
		RecordEntity record = recordService.getById(id);

        return R.ok().put("record", record);
    }

    /**
     * 保存
     */
//    @RequiresPermissions("app:record:save")
    @RequestMapping("/save")
    public R save(@RequestBody RecordEntity record){
		recordService.save(record);

        return R.ok();
    }

    /**
     * 修改
     */
//    @RequiresPermissions("app:record:update")
    @RequestMapping("/update")
    public R update(@RequestBody RecordEntity record){
		recordService.updateById(record);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @RequiresPermissions("app:record:delete")
    public R delete(@RequestBody Map<String, Object> params){
//        Long[] idss= (Long[]) ids;
//        Long[] ids2=new Long[ids.length];
//
//        for (int i = 0; i < ids.length; i++) {
//            System.out.println("ids++++========="+ids[i]);
//            ids2[i]= (Long) ids[i];
//        }
//
//        recordService.deleteBatch(ids2);
//        System.out.println("ids======"+ids);
//		recordService.removeByIds(Arrays.asList(ids));
        recordService.deleteBatch(params);
        return R.ok();
    }

}
