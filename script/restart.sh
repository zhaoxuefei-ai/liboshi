#!/bin/bash
mv highway_calculator_fast.jar highway_calculator_fast.jar.$(date +%Y%m%d)
mv highway_calculator_fast_*.jar highway_calculator_fast.jar
kill -9 $( lsof -i:8020 -t )
echo 'process in 8020 has been killed'
#rm -f nohup.out
nohup java -jar highway_calculator_fast.jar &
echo 'highway_calculator_fast in port 8020'
sleep 1
tail -f nohup.out
