package com.wdjt.highwayCalculatorFast;


import com.corundumstudio.socketio.SocketIOServer;
import com.wdjt.highwayCalculatorFast.common.utils.Constant;
import com.wdjt.highwayCalculatorFast.common.utils.IPUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

// 初始化常数
@Component
public class StartUp implements CommandLineRunner {
    private static final Logger log = LogManager.getLogger(StartUp.class);

    @Autowired
    Environment environment;

    @Autowired
    private SocketIOServer server;


    @Override
    public void run(String... args) {
        // 初始化
        String port = environment.getProperty("server.port");
        String mode = environment.getProperty("spring.profiles.active");
        String ip = "";
        if (mode.equals("dev")) {
            ip = "localhost";
        } else if (mode.equals("prod")) {
            ip = IPUtils.getLocalV4IP();
        }
        Constant.ip = ip;
        Constant.port = port;
        log.info("Server running in : " + Constant.ip + ":" + Constant.port);

        server.start();
        log.info("websocket 服务器启动成功。。。");
    }
}
