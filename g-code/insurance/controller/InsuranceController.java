package com.wdjt.highwayCalculatorFast.modules.insurance.controller;

import java.util.Arrays;
import java.util.Map;

import com.wdjt.highwayCalculatorFast.common.utils.MapUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wdjt.highwayCalculatorFast.modules.insurance.entity.InsuranceEntity;
import com.wdjt.highwayCalculatorFast.modules.insurance.service.InsuranceService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.R;



/**
 * 保险反欺诈系统 案件表 包括 档案组 (case是java 保留字 改为 insurance)
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 10:54:45
 */
@RestController
@RequestMapping("insuranceList/insurance")
public class InsuranceController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private InsuranceService insuranceService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("insuranceList:insurance:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = insuranceService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 列表
     */
    @RequestMapping("/list/adv")
    @RequiresPermissions("insuranceList:insurance:list")
    public R listAdv(@RequestBody Map<String, Object> params){
        PageUtils page = insuranceService.queryPageAdv(params);

        return R.ok().put("page", page);
    }
    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("insuranceList:insurance:info")
    public R info(@PathVariable("id") Integer id){
		InsuranceEntity insurance = insuranceService.getById(id);

        return R.ok().put("insurance", insurance);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("insuranceList:insurance:save")
    public R save(@RequestBody InsuranceEntity insurance){
		insuranceService.save(insurance);
        return R.ok(new MapUtils().put("id",insurance.getId()));
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("insuranceList:insurance:update")
    public R update(@RequestBody InsuranceEntity insurance){
		insuranceService.updateById(insurance);
        return R.ok(new MapUtils().put("id",insurance.getId()));
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("insuranceList:insurance:delete")
    public R delete(@RequestBody Integer[] ids){
		insuranceService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
