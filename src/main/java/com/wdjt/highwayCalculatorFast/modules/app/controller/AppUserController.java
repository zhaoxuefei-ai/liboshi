package com.wdjt.highwayCalculatorFast.modules.app.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import com.wdjt.highwayCalculatorFast.common.validator.Assert;
import com.wdjt.highwayCalculatorFast.common.validator.ValidatorUtils;
import com.wdjt.highwayCalculatorFast.modules.app.annotation.Login;
import com.wdjt.highwayCalculatorFast.modules.app.annotation.LoginUser;
import com.wdjt.highwayCalculatorFast.modules.app.form.RegisterForm;
import com.wdjt.highwayCalculatorFast.modules.sys.form.PasswordForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.wdjt.highwayCalculatorFast.modules.app.entity.UserEntity;
import com.wdjt.highwayCalculatorFast.modules.app.service.UserService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.common.utils.R;


/**
 * 用户
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-17 16:40:43
 */
@RestController
@RequestMapping("/app/user")
@Api("APP user接口")
public class AppUserController {
    @Autowired
    private UserService userService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("app:user:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = userService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息 后台
     */
    @RequestMapping("/info/{userId}")
    @RequiresPermissions("app:user:info")
    public R info(@PathVariable("userId") Long userId) {
        UserEntity user = userService.getById(userId);

        return R.ok().put("user", user);
    }


    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("app:user:save")
    public R save(@RequestBody RegisterForm form) {
        //表单校验
        ValidatorUtils.validateEntity(form);

        UserEntity user = new UserEntity();
        // 修改 公司名称
        user.setUsername(form.getUsername());
        user.setCompany(form.getCompany());
        user.setPassword(DigestUtils.sha256Hex(form.getPassword()));
        user.setCreateTime(new Date());
        user.setExpireTime(form.getExpireTime());
        // 增加 计算器时间 网段过滤  有默认值
        user.setCalExpireTimeList(form.getCalExpireTimeList());
        user.setIpRangeList(form.getIpRangeList());
        user.setRemark(form.getRemark());
        user.setUseTime(0);
        userService.save(user);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("app:user:update")
    public R update(@RequestBody UserEntity user) {
        userService.updateById(user);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("app:user:delete")
    public R delete(@RequestBody Long[] userIds) {
        userService.removeByIds(Arrays.asList(userIds));

        return R.ok();
    }

    // app
    @Login
    @GetMapping("userInfo")
    @ApiOperation("获取用户信息")
    public R userInfo(@LoginUser UserEntity user) {
        return R.ok().put("user", user);
    }

    @Login
    @GetMapping("userId")
    @ApiOperation("获取用户ID")
    public R userInfo(@RequestAttribute("userId") Long userId) {
        return R.ok().put("userId", userId);
    }

    /**
     * 修改登录用户密码
     */
    @Login
    @PostMapping("/password")
    public R password(@RequestBody PasswordForm form,
                      @RequestAttribute("userId") Long userId) {
        Assert.isBlank(form.getNewPassword(), "新密码不为能空");

        //sha256加密
        String password = DigestUtils.sha256Hex(form.getPassword());
        //sha256加密
        String  newPassword = DigestUtils.sha256Hex(form.getNewPassword());

        //更新密码
        boolean flag = userService.updatePassword(userId, password, newPassword);
        if (!flag) {
            return R.error("原密码不正确");
        }

        return R.ok();
    }
}
