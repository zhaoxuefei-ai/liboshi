/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package com.wdjt.highwayCalculatorFast.modules.app.interceptor;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wdjt.highwayCalculatorFast.common.exception.RRException;
import com.wdjt.highwayCalculatorFast.common.utils.Constant;
import com.wdjt.highwayCalculatorFast.modules.app.entity.UserEntity;
import com.wdjt.highwayCalculatorFast.modules.app.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Map;

/**
 * 权限(Calculator)验证
 *
 * @author panzetao
 */
@Component
public class CalculatorInterceptor extends HandlerInterceptorAdapter {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;

    public static final Map<String, String> ENGLISHTOCHINA = Constant.englishToChina;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Long userId = (Long) request.getAttribute(AuthorizationInterceptor.USER_KEY);
        if (userId==null) {
            throw new RRException("用户不存在", HttpStatus.UNAUTHORIZED.value());
        }
        UserEntity user = userService.getById(userId);
        String calExpireTimeList = user.getCalExpireTimeList();
        JSONArray jsonArray = JSON.parseArray(calExpireTimeList);
        String calEnName = "";

        for (Object o : jsonArray) {
            JSONObject jsonObject = (JSONObject) o;
            if (ENGLISHTOCHINA.get(calEnName).equals(jsonObject.getString("name"))) {
                Date expire = jsonObject.getDate("expire");
                // 时间是空的
                if (expire == null) {
                    throw new RRException("计算器未授权", HttpStatus.UNAUTHORIZED.value());
                }
                // 非空但是过时了
                if (expire.before(new Date())) {
                    throw new RRException("计算器已过期", HttpStatus.UNAUTHORIZED.value());
                }
                // 还可以使用
                if (expire.after(new Date())) {
                    return true;
                }
            }
        }

        // 默认进不去
        return false;
    }
}
