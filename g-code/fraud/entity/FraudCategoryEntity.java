package com.wdjt.highwayCalculatorFast.modules.fraud.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * 案件的关键信息 规则结果表
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-08-23 18:19:04
 */
@Data
@TableName("t_fraud_category")
public class FraudCategoryEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Integer id;
    /**
     * 父级欺诈种类id
     */
    private Integer parentId;
    /**
     * 欺诈条目名称
     */
    private String fraudName;
    /**
     * 1级标签的英文名 (前端好处理
     */
    private String baseEnglishName;

    @TableField(exist = false)
    private List<FraudCategoryEntity> childrenList;


    public String[] getChildrenNames() {
        List<String> childrenNames = new ArrayList<>();
        if (this.childrenList != null) {
            for (FraudCategoryEntity fraudCategoryEntity : this.childrenList) {
                childrenNames.add(fraudCategoryEntity.getFraudName());
            }
        }
        String[] strings = new String[childrenNames.size()];
        childrenNames.toArray(strings);
        return strings;
    }
}
