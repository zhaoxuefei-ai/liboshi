/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package com.wdjt.highwayCalculatorFast.modules.app.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.modules.app.entity.CalculatorEntity;
import com.wdjt.highwayCalculatorFast.modules.app.entity.UserEntity;
import com.wdjt.highwayCalculatorFast.modules.app.form.LoginForm;

import java.util.List;
import java.util.Map;

/**
 * 用户
 *
 * @author Mark sunlightcs@gmail.com
 */
public interface UserService extends IService<UserEntity> {

	UserEntity queryByUsername(String mobile);

	/**
	 * 用户登录
	 * @param form    登录表单
	 * @return        返回用户ID
	 */
	long login(LoginForm form);

	PageUtils queryPage(Map<String, Object> params);


	List<CalculatorEntity> queryAllCalculator(Long userId);

    boolean updatePassword(Long userId, String password, String newPassword);


	void updateHeartbeat(Long userId, Long t);
}
