package com.wdjt.highwayCalculatorFast.modules.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wdjt.highwayCalculatorFast.common.utils.PageUtils;
import com.wdjt.highwayCalculatorFast.modules.user.entity.UserEntity;

import java.util.Map;

/**
 * 用户
 *
 * @author panzetao
 * @email panzetaoyx@qq.com
 * @date 2019-09-17 16:40:43
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

